#!/usr/bin/env python3
# Standard library modules.

# Third party modules.

# Local modules
import mosquito

# Globals and constants variables.


with mosquito.swarm() as scheduler:
    result = scheduler.get('https://httpbin.org/get', params=dict(message='hello world'))
    print(result.json().get('args').get('message'))