#!/usr/bin/env python3
# Standard library modules.
import time
import queue
import logging
import threading

logger = logging.getLogger('mosquito')
logger.setLevel(logging.INFO)

# Third party modules.

# Local modules
import mosquito
from mosquito.tests import httpbin

# Globals and constants variables.
QUEUE = mosquito.MonitoredQueue()
RECEIVED_BYTES = 0


# identity specific headers
@mosquito.attribute('headers')
def load_headers():
    logger.debug(f'load header data')

    for ua in ['foo', 'bar', 'baz', 'bam', 'fnord']:
        yield {'user-agent': ua}


# identity specific login credentials
@mosquito.attribute('auth')
def load_logins():
    logger.debug(f'load authentication information')

    for i, name in enumerate(['Anna', 'Boob', 'Carl', 'Dave', 'Fool', 'Gerd']):
        yield name, f'pwd_{i}'


# callback for initializing sessions (e.g. to log in)
@mosquito.attribute('on_init')
def init_callback(session):
    logger.debug(f'initialize {session}')

    session.get(httpbin(f'/cookies/set/sid/{session.id}'))


# register hooks that inspect responses and count received bytes
@mosquito.attribute('hooks')
def hook_factory():
    def _bytes(response, **kwargs):
        global RECEIVED_BYTES
        RECEIVED_BYTES += len(response.content)

    for i in range(6):
        yield {'response': [_bytes]}


# don't delay sessions, make use of authentication mandatory (just a self check)
mosquito.register_attributes(delay=.0, require=['auth'])


def run():
    try:
        with mosquito.swarm() as scheduler:
            while True:
                url = QUEUE.get(block=False)
                response = scheduler.get(url, params=dict(bar=42))
                QUEUE.task_done()

                logger.debug(f'{url}, {response.url}, {response.json()}')

    except (mosquito.MosquitoError, AttributeError) as error:
        logger.warning(f'{type(error).__name__}: {error}')

    except queue.Empty:
        pass

    logger.debug('terminate')


# populate request queue
for url in [httpbin(f'/anything?foo={i}') for i in range(1000)]:
    QUEUE.put(url)

# create worker threads
threads = [threading.Thread(name=f'worker_{i}', target=run) for i in range(10)]
logger.info(f'start {len(threads)} threads, {QUEUE.qsize()} requests in queue')

# start threads
for thread in threads:
    thread.start()

time.sleep(.2)

# display progress while waiting for threads to terminate
with QUEUE.tqdm(desc='queue', unit='requests'):
    for thread in threads:
        thread.join()

time.sleep(.2)

# print some statistics
logger.info(mosquito.observer())
logger.info(f'received {RECEIVED_BYTES} bytes')
